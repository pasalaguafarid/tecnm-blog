import { useDispatch, useSelector } from "react-redux";

import {
    onChecking,
    onClearErrorMessage,
    onLogin,
    onLogout,
} from "../store/auth/authSlice";
import { blogApi } from "@/api";
import { RootState } from "@/store";

export const useAuthStore = () => {
    const { status, user, errorMessage } = useSelector((store: RootState) => store.auth);

    const dispatch = useDispatch();

    const startLogin = async ({ email, password }: { email: string, password: string }) => {
        
        dispatch(onChecking());
        try {

            const resp = await blogApi.post("/auth/login", { email, password });

            console.log(resp);
            

            // localStorage.setItem("token", data.token);

            // dispatch(onLogin(data.user));
        } catch (error) {
            dispatch(onLogout("Check the email and password"));
            setTimeout(() => {
                dispatch(onClearErrorMessage());
            }, 10);
        }
    };

    const startRegistering = async ({ name, email, password }: { name: string, email: string, password: string }) => {
        dispatch(onChecking());
        try {
            const { data } = await blogApi.post("/auth/new", {
                name,
                email,
                password,
            });

            localStorage.setItem("token", data.token);

            dispatch(onLogin({ name: data.name, uid: data.uid }));
        } catch (error: any) {
            dispatch(onLogout(error.response?.data?.msg || ""));
            setTimeout(() => {
                dispatch(onClearErrorMessage());
            }, 10);
        }
    };

    const checkAuthToken = async () => {
        const token = localStorage.getItem("token");
        if (!token) return dispatch(onLogout(""));

        try {
            const { data } = await blogApi.get("auth/renew");
            localStorage.setItem("token", data.token);
            dispatch(onLogin({ name: data.name, uid: data.uid }));
        } catch (error) {
            localStorage.clear();
            dispatch(onLogout(""));
        }
    };

    const startLogOut = async () => {
        localStorage.clear();
        dispatch(onLogout(""));
    };

    return {
        status,
        user,
        errorMessage,

        startLogin,
        startRegistering,
        checkAuthToken,
        startLogOut,
    };
};