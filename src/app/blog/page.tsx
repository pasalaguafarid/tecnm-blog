import Image from "next/image";

const BlogPage = () => {
    return (
        <main className="mh-100">
            <section className="mt-20 mh-20 grid-c-4 gap-25">
                <div>
                    <Image src='/resources/images/test.jpg' alt="" width={500} height={300} className="max-width" />
                    <p className="f-size-24 mt-20 bold">Google lanza el futuro de los videojuegos</p>
                    <p className="gray-color mt-10 gray-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos facilis, praesentium quaerat commodi.</p>
                    <p className="bold mt-20 f-size-14">Luis leal</p>
                    <p className="f-size-14 gray-text">Marzo 20, 2023.</p>
                </div>
                <div>
                    <Image src='/resources/images/test.jpg' alt="" width={500} height={300} className="max-width" />
                    <p className="f-size-24 mt-20 bold">Google lanza el futuro de los videojuegos</p>
                    <p className="gray-color mt-10 gray-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos facilis, praesentium quaerat commodi.</p>
                    <p className="bold mt-20 f-size-14">Luis leal</p>
                    <p className="f-size-14 gray-text">Marzo 20, 2023.</p>
                </div>
                <div>
                    <Image src='/resources/images/test.jpg' alt="" width={500} height={300} className="max-width" />
                    <p className="f-size-24 mt-20 bold">Google lanza el futuro de los videojuegos</p>
                    <p className="gray-color mt-10 gray-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos facilis, praesentium quaerat commodi.</p>
                    <p className="bold mt-20 f-size-14">Luis leal</p>
                    <p className="f-size-14 gray-text">Marzo 20, 2023.</p>
                </div>
                <div>
                    <Image src='/resources/images/test.jpg' alt="" width={500} height={300} className="max-width" />
                    <p className="f-size-24 mt-20 bold">Google lanza el futuro de los videojuegos</p>
                    <p className="gray-color mt-10 gray-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos facilis, praesentium quaerat commodi.</p>
                    <p className="bold mt-20 f-size-14">Luis leal</p>
                    <p className="f-size-14 gray-text">Marzo 20, 2023.</p>
                </div>
            </section>
        </main>
    )
}

export default BlogPage;