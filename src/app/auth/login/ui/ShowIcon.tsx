


export const ShowIcon = ({ show, inp, showPassword }: { show: boolean, inp: any, showPassword: any }) => {

    return (
        <i className={(show) ? 'fa-solid fa-eye' : 'fa-solid fa-eye-slash'} onClick={() => (showPassword({
            show: !show,
            inp: inp
        }))} id="password-icon"></i>
    )
}