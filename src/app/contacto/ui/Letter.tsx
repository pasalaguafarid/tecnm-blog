import Image from 'next/image'

const Letter = () => {
    

    return(
        <div style={{zIndex: '5'}}>
            <div style={{zIndex: '6', margin: '0px', padding: '0px', border: '0px'}} className='absolute bg-white white-border radius shadow' id='letter'>

                <div style={{width: '100%', height: '10dvh', padding: '4%'}} className='bg-primary radius-top flex'>
                    <div>
                        <i className='fa-solid fa-envelope relative'
                        style={{marginTop: '3%', marginLeft: '5%', fontSize: '5vh'}}></i>

                    </div>
                    
                    
                    <span style={{fontSize: '1.7vh', margin: '3%', height: '4dvh'}}>Escríbenos un correo electronico para atender a tu duda lo antes posible</span>

                </div>
                

                <div style={{marginTop: '2dvh', height: '80%'}} className='flex f-wrap gap-25 center'>
            
                    <input style={{minWidth: '27dvh', maxHeight: '5vh'}} type='text' className='input' placeholder='Tu nombre'/>

                    <input style={{minWidth: '27dvh', maxHeight: '5vh'}} className='input' placeholder='Correo'></input>

                    <input style={{minWidth: '27dvh', maxHeight: '5vh'}} className='input' placeholder='Semestre'></input>

                    <input style={{minWidth: '27dvh', maxHeight: '5vh'}} className='input' placeholder='Carrera'></input>


                    <textarea className='input' placeholder='Escribe aquí tu duda...' cols={60} rows={10}
                    style={{minWidth: '57dvh', minHeight: '20dvh', resize: 'none', maxWidth: '57dvh'}}></textarea>



                    <input type='file' 
                    style={{minWidth: '57dvh'}} className='input input-file'></input>


                    <button  type='submit' className='p-10 radius white-border black-text bg-secondary' style={{minWidth: '57dvh', marginTop: '15px', maxHeight: '5vh'}} >Send</button>
                </div>



            </div>


        </div>
    )
}

export default Letter