import React from 'react'
import Letter from './ui/Letter'

const ContactoPage = () => {


    return (
        <section className='flex column total-width' style={{height: '105dvh'}}>
            <div className='flex column bg-secondary total-width shadow-2' style={{zIndex: '1', height: '40vh'}}>

                <div className='mh-40'>
                    <span className='white-text bold f-size-9vh'>Contáctanos</span>
                    <br />
                    <span className='pt-50 white-text f-size-4vh bold'>¿Tienes alguna </span>
                    <span className='pt-50 text-secondary f-size-4vh bold border-font'>duda</span>
                    <span className='pt-50 white-text f-size-4vh bold'>?</span>
                    <br />
                    <span className='white-text f-size-4vh bold'>Háznoslo saber</span>
                </div>

                <div className='relative flex white-text f-size-4vh' style={{margin: '30px 60px', marginTop: 'auto', gap: '5vh'}}>
                    <a href="https://celaya.tecnm.mx/" className='white-text' target='_blank'>
                        <i className='cursor-pointer fa-solid fa-globe' ></i>
                    </a>
                    <a href="https://www.instagram.com/tecnmencelaya?igsh=Z3h2N2xzNXRwNzl0" className='white-text' target='_blank'>
                        <i className='cursor-pointer fa-brands fa-instagram'></i>
                    </a>

                    {/* <a href="" className='white-text' target='_blank'>
                        <i className='cursor-pointer fa-brands fa-whatsapp'></i>
                    </a> */}
                </div>

            </div>

            <Letter/>
            

            <div className='bg-primary total-width f-grow4' style={{zIndex: '-2'}}>
                <div className='mh-40 mt-10'>
                    <span className='text-primary bold f-size-8vh border-font'>¿Cuál es el siguiente paso?</span>
                    <br />
                    <span></span>
                </div>


                <div className="container-fluid mh-40 f-size-1-9vh" style={{width: '400px'}}>
                    <br /><br />
                    <ul className="multi-steps">
                        <li>
                            <div className="step-content">
                            <div className="step-title">Identifica</div>
                            <div className="step-description">A quién o que departamento va dirigida.</div>
                            </div>
                        </li>

                        <li>
                            <div className="step-content">
                            <div className="step-title">Envía</div>
                            <div className="step-description">Llanados los datos revisaremos tu duda.</div>
                            </div>
                        </li>

                        <li>
                            <div className="step-content">
                            <div className="step-title">Respuesta</div>
                            <div className="step-description">Recibirás un correo de respuesta a tu duda.</div>
                            </div>
                        </li>
                    </ul>

                </div> 

            </div> 

        </section>
    )
}

export default ContactoPage