import Link from "next/link"

export const Footer = () => {
    return (
        <footer className="mh-150 mt-50">
            {/* <div>
                <Image src='' width={500} height={500} alt="" />
                <div>

                </div>
            </div> */}
            <hr />
            <div className="center-text gray-text flex column gap-15 mt-50 mb-50">
                <p>TecBlog © Copyright {new Date().getFullYear()} Todos los derechos reservados.</p>
                <Link href='https://itcoders.tech/' target="_blank" className="gray-text">itcoders.tech</Link>
            </div>
        </footer>
    )
}
