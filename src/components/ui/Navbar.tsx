import Link from "next/link"

export const Navbar = () => {
  return (
    <nav className="flex align-center space-between mh-150 mt-50 mb-50">
      <Link href='/' className="black-text"><h1>TecBlog</h1></Link>
      <div className="flex align-center gap-25">
        <input type="text" placeholder="Buscar..." className="input" />
        <i className="fa-regular fa-user f-size-30"></i>
      </div>
    </nav>
  )
}
